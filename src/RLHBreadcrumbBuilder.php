<?php

/**
 * @file
 * Contains \Drupal\rlh_breadcrumb\BreadcrumbBuilder.
 */

namespace Drupal\rlh_breadcrumb;

//use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\system\PathBasedBreadcrumbBuilder;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Url;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

class RLHBreadcrumbBuilder extends PathBasedBreadcrumbBuilder {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    /*
     *
     if ($attributes['_route'] == 'node_page') {
      return $attributes['node']->bundle() == 'article';
    }
    */
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    \Drupal::logger('rlh_breadcrumb')->info("Entering build");
    \Drupal::logger('rlh_breadcrumb')->info("Referer is @referer", ['@referer' => print_r($_SERVER['HTTP_REFERER'], TRUE)]);
    $breadcrumb = null;
    $refererPath = $_SERVER['HTTP_REFERER'];

    $request = \Drupal::request();
    $path = trim($this->context->getPathInfo(), '/');
    $path_elements = explode('/', $path);
    $route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);

    if ($route && !$route->getOption('_admin_route')) {
      //TODO: Need to add a check to see if the node has a flag set for using the special breadcrumb
      if ($refererPath) {
        $exclude = array();
        $refererRequest = parent::getRequestForPath($refererPath, $exclude);
        if ($refererRequest) {
          //If we are in the special case, use the referer to build the breadcrumb, rather than using
          //the current path
          $refererRoute = $refererRequest->attributes->get(RouteObjectInterface::ROUTE_OBJECT);
          $route_match = RouteMatch::createFromRequest($refererRequest);
          $breadcrumb = parent::build($route_match);
          $refererTitle = $this->titleResolver->getTitle($refererRequest, $refererRoute);
        }

        if (!isset($refererTitle)) {
          // Fallback to using the raw path component as the title if the
          // route is missing a _title or _title_callback attribute.
          $refererTitle = str_replace(array('-', '_'), ' ', Unicode::ucfirst(end($path_elements)));
        }
        //$breadcrumb->addLink(Link::createFromRoute($refererTitle, $refererRequest->get('_route'), $refererRequest->get('node')));
        //$breadcrumb->addLink(Link::createFromRoute($refererTitle, $route_match->getRouteName(), $route_match->getParameters()->all()));
        $breadcrumb->addLink(Link::fromTextAndUrl($refererTitle, Url::fromUserInput($refererRequest->getRequestUri())));
        //$refererRequest->attributes['param']
      }
      else {
        $breadcrumb = parent::build($route_match);
      }
      $title = $this->titleResolver->getTitle($request, $route);
      if (!isset($title)) {
        // Fallback to using the raw path component as the title if the
        // route is missing a _title or _title_callback attribute.
        $title = str_replace(array('-', '_'), ' ', Unicode::ucfirst(end($path_elements)));
      }
      $breadcrumb->addLink(Link::createFromRoute($title, '<none>'));


      $breadcrumb->addCacheableDependency(NULL);
    }
    else {
      $breadcrumb = parent:: build($route_match);
    }
    return $breadcrumb;
  }
}
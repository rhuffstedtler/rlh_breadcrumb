This is my little sandbox module to play with some breadcrumb concepts that
we need to incorporate into a customer site.

Background information for this module can be found in the change record
that introduced the new breadcrumb system (https://www.drupal.org/node/2026025)
and in this post (http://www.gregboggs.com/drupal8-breadcrumbs/) by Greg Boggs,
the creator of the Easy Breadcrumb module.

